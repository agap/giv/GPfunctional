% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/fct_BayesRCpiplus.R
\name{BayesRCpiplus}
\alias{BayesRCpiplus}
\title{BayesRCpiplus}
\usage{
BayesRCpiplus(
  geno,
  map,
  pheno,
  mk_class,
  soft_exc_loc = "./software/bayesRCO",
  prior_SNP_var = 0.01,
  prior_err_var = 0.01,
  df_SNP_var = NULL,
  df_err_var = NULL,
  prior_prop_Dirichlet = 1,
  msize = 0,
  mrep = 5000,
  numit = 1000,
  burnin = 500,
  thin = 10,
  ndist = 4,
  var_prop_mix = c(0, 1e-04, 0.001, 0.01),
  seed = 0,
  predict = FALSE,
  additive = FALSE
)
}
\arguments{
\item{geno}{genotype marker score matrix with genotype as row and marker as
columns. Marker scores must be scored 0, 1, 2 corresponding to the allele
frequency on one allele (generally minor).}

\item{map}{corresponding marker map data frame with marker names chromosome
genetic position, physical position.}

\item{pheno}{numeric vector of phenotypic values.}

\item{mk_class}{List of the marker position (numeric) organised per class.
One element per marker class.}

\item{soft_exc_loc}{\code{character} string giving the path to the bayesR
executable file with data and result folder architecture. Default =
"./software/bayesR"}

\item{prior_SNP_var}{Prior variance of the SNP effect (Va). parameter from a scaled
inverse chi-squared distribution. Default = 0.01}

\item{prior_err_var}{Prior variance of the error (Ve). parameter from a scaled
inverse chi-squared distribution. Default = 0.01}

\item{df_SNP_var}{degree of freedom of Va in the scaled inverse chi-squared
distribution. Problem with direct assignment of negative values. Default = -2}

\item{df_err_var}{degree of freedom of Ve in the scaled inverse chi-squared
distribution. Problem with direct assignment of negative values. Default = -2}

\item{prior_prop_Dirichlet}{vector of delta parameter for the mixture proportion
Dirichlet prior distribution. Default = c(1, 1, 1, 1).}

\item{msize}{number of SNPs in reduced update. Default = 0}

\item{mrep}{number of full cycles in reduced update. Default = 5000}

\item{numit}{length of MCMC chain. Default = 5000}

\item{burnin}{burnin steps. Default = 1000}

\item{thin}{thinning rate. Default = 10}

\item{ndist}{number of mixture distributions}

\item{var_prop_mix}{proportion of the genetic variance explained by the
different mixture distribution. Default = c(0.0,0.0001,0.001,0.01).}

\item{seed}{initial value for random number. Default = 0}

\item{predict}{\code{logical} value specifying if prediction
should be realized. Default = TRUE.}

\item{additive}{\code{logical} value specifying if BayesRCpi (FALSE) or
BayesRC+ (TRUE) should be realized. Default = FALSE for BayesRCpi.}

\item{covar}{optional design matrix for covariates. Default = NULL}
}
\value{
list with  : marker effects (B_mk),
marker effect class frequencies (B_cl_freq),
genetic value predictions (g)
}
\description{
Function to calculate BayesRCpi or BayesRC+ model using
the BayesRCO program from Mollandin et al. (2022).
}
\examples{

\dontrun{
data("geno", package = "GPfunctional")
data("map", package = "GPfunctional")
# phenotypes simulated
pheno_sim <- GPF_sim_pheno(X = geno, map = map)

# set working directory with bayesR executable file
setwd("C:/Users/lacie/OneDrive/Documents/M2 DATA ANALYST/STAGE/BCNAM_Data/GP_functional")

## Non-overlapping annotations :
# List of the marker position (numeric) organised per class :
mk_class <- list(c1 = which(map$mk.names \%in\% pheno_sim$mk_sel_f$mk.names),
                 c2 = which(!(map$mk.names \%in\% pheno_sim$mk_sel_f$mk.names)))


## Overlapping annotations (here, more than 2 classes) :
cl1 <- sample(1:ncol(geno), 100)
cl2 <- c(1:100)
cl3 <- (1:ncol(geno))[-cl1]

mk_class <-list(c1 = cl1,
                c2 = cl2,
                c3 = cl3)

## BayesRCpi :

### without prediction :
res_BayesRCpi <- BayesRCpiplus(geno = geno, map = map,
                               pheno = pheno_sim$d_y$y_sim,
                               mk_class = mk_class,seed = 2108,
                               predict = FALSE)

g_hat <- res_BayesRCpi$g

# breeding values simulated :
bv_sim <- pheno_sim$d_y$y_f + pheno_sim$d_y$y_r

plot(x = g_hat[, 1],y = bv_sim)
cor(x = g_hat[, 1],y = bv_sim)

### with prediction :
bv_sim2 <- bv_sim
set.seed(2108)
nbalea<-sample(1:100,20)
bv_sim2[nbalea]<-NA # phenotypes to predict are encoded 'NA'

res_BayesRCpi_pred <- BayesRCpiplus(geno = geno, map = map,
                                    pheno = pheno_sim$d_y$y_sim,
                                    mk_class = mk_class,seed = 2108,
                                    predict = TRUE)
g_hat2 <- res_BayesRCpi_pred$g
plot(x = g_hat2[nbalea, 1],y = bv_sim[nbalea])
cor(x = g_hat2[nbalea, 1],y = bv_sim[nbalea])
# cor between breeding values predicted and breeding values simulated

## BayesRC+:

### without prediction :
res_BayesRCplus <- BayesRCpiplus(geno = geno, map = map,
                                 pheno = pheno_sim$d_y$y_sim,
                                 mk_class = mk_class,seed = 2108,
                                 predict = FALSE,
                                 additive = TRUE)

g_hat <- res_BayesRCplus$g

# breeding values simulated :
bv_sim <- pheno_sim$d_y$y_f + pheno_sim$d_y$y_r

plot(x = g_hat[, 1],y = bv_sim)
cor(x = g_hat[, 1],y = bv_sim)

### with prediction :
bv_sim2 <- bv_sim
set.seed(2108)
nbalea<-sample(1:100,20)
bv_sim2[nbalea]<-NA # phenotypes to predict are encoded 'NA'

res_BayesRCplus_pred <- BayesRCpiplus(geno = geno, map = map,
                                      pheno = pheno_sim$d_y$y_sim,
                                      mk_class = mk_class,seed = 2108,
                                      additive = TRUE,
                                      predict = TRUE)
g_hat2 <- res_BayesRCplus_pred$g
plot(x = g_hat2[nbalea, 1],y = bv_sim[nbalea])
cor(x = g_hat2[nbalea, 1],y = bv_sim[nbalea])
# cor between breeding values predicted and breeding values simulated
}

}
\references{
Mollandin et al. (2022) Accounting for overlapping annotations in
genomic prediction models of complex traits. BMC Bioinformatics,
23(365). https://doi.org/10.1186/s12859-022-04914-5
}
