% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/fct_write_gen_var_prior.R
\name{write_gen_var_prior}
\alias{write_gen_var_prior}
\title{write_gen_var_prior}
\usage{
write_gen_var_prior(V_gen_prior = matrix(c(0.001, 0.001, 4, 4), 2), path)
}
\arguments{
\item{V_gen_prior}{\code{Numeric matrix} containing the parameter of the
SNP class genetic variance prior distribution. Each genetic variance. is
described by a scaled inverse chi-squared distribution
\eqn{scale-\chi^{-2}(\nu, \tau^{2})}.
Each row correspond to the genetic variance associated to one SNP class.
The first column represent the scaling parameter \eqn{\tau^{2}}.
The second column represents the degree of freedom \eqn{\nu}.
Default = matrix(c(0.001, 0.001, 4, 4), 2)}

\item{path}{\code{character} string specifying the path where data need
to be saved.}
}
\value{
varcomp file written at the path location
}
\description{
Write file describing parameter of the scaled inverse
chi-squared distribution for the genetic effect of the prior
}
\examples{

\dontrun{
V_gen_prior <- matrix(c(0.05, 4.0, 0.001, 4.0), nrow = 2, byrow = TRUE)
write_gen_var_prior(V_gen_prior, path = "./software/bayesR/data")
}

}
