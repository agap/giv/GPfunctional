% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/fct_write_SNP_classes_prior.R
\name{write_SNP_classes_prior}
\alias{write_SNP_classes_prior}
\title{write_SNP_classes_prior}
\usage{
write_SNP_classes_prior(SNP_classes_prior, path)
}
\arguments{
\item{SNP_classes_prior}{\code{list} with each element corresponding to the
normal mixture information of a SNP class. Each element is a vector specifying:
number of components, the proportions of genetic variance explained (\% VG_SNP_i)
and the prior of the Dirichlet distribution. See notes for details.}

\item{path}{\code{character} string specifying the path where data need
to be saved.}
}
\value{
The return value, if any, from executing the function.
}
\description{
Write file describing prior distributions of the normal mixtures
corresponding to each SNP effect classes.
}
\note{
The mixture distributions specified in \verb{param SNP_classes_prior} are specified
using a vector taking the following information c(number of components,
prop var cl1, prop variance cl2, ..., Dirichlet delta prior).
For example, c(4, 0, 0.01, 0.001, 0.0001, 1,1,1,1), correspond to the default mixture
prior of the BayesR function with

\eqn{p(\beta_j|\bm{\pi}, \sigma_g^{2}) = \pi_1 \times N(0, 0 \times \sigma_g^{2}) + ... +
\pi_4 \times N(0, 0.0001 \times \sigma_g^{2})}.

and

\eqn{
	p(\pi_1, \pi_2, \pi_3, \pi_4) \sim D(1, 1, 1, 1)
}
}
\examples{
SNP_classes_prior <- list(cl_1 = c(2, 0, 0.01, 1,1),
cl_2 = c(4, 0, 0.01, 0.001, 0.0001, 1,1,1,1))

\dontrun{
write_SNP_classes_prior(SNP_classes_prior = SNP_classes_prior,
path = "./software/bayesR/data")
}

}
