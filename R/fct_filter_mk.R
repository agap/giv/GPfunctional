#' filter_mk
#'
#' @description Filters markers that segregate, i.e.,
#' whose allele frequency is higher or equal than a defined
#' threshold in at least one of the crosses or in all crosses.
#'
#' @param geno \code{numeric} genotype marker matrix with genotype x marker
#' information coded as 0, 1, 2 representing the number of copies of the
#' minor allele.
#'
#' @param map genetic marker map: marker id, chromosome, genetic position,
#' physical position.
#'
#' @param threshold \code{numeric} threshold for filtering the
#' minimal allele frequency necessary to retain the markers.
#' Default = 0.1
#'
#' @param mk_commun \code{logical} FALSE to select markers that segregate
#' above the threshold in at least one cross.
#' TRUE to select ONLY markers that segregate above the threshold
#' in ALL crosses. Default = FALSE.
#'
#'
#' @return list containing the following object :
#'
#' 1. geno_sel: Genotype matrix with only the markers
#' that segregate above the threshold (selected markers).
#' 2. map_sel: Map of the segregating markers (same as above).
#' 3. nb_mk_cross: Table with as many rows as the number of crosses,
#' where the first column corresponds to the number of individuals
#' in that cross and the second column to the number of markers
#' that segregate above the threshold in that cross.
#'
#' @examples
#'
#' load("~/M2 DATA ANALYST/STAGE/BCNAM_Data/Scripts_Data/Global_map.RData")
#' load("~/M2 DATA ANALYST/STAGE/BCNAM_Data/Scripts_Data/geno_012.RData")
#' res_filt <- filter_mk(geno = geno_012, map = map,
#' threshold = 0.1)
#'
#' @export
#'

# load("~/M2 DATA ANALYST/STAGE/BCNAM_Data/Scripts_Data/Global_map.RData")
# load("~/M2 DATA ANALYST/STAGE/BCNAM_Data/Scripts_Data/geno_012.RData")
# geno<-geno_012
# threshold <- 0.1
# mk_commun <- TRUE

filter_mk <- function (geno, map, threshold = 0.1,
                       mk_commun = FALSE){

  cr_ind <- substr(x = rownames(geno), start = 1, stop = 4)
  cr_id <- unique(cr_ind)
  n_cr <- length(cr_id)

  list_fq <- vector(mode = "list", length = n_cr)
  nb_mk_cross <- data.frame()

  # calculer les fq des mk dans chak croisement
  for (i in 1:n_cr){

    geno_i<-geno[cr_ind %in% cr_id[i], ]

    nb_mk_cross[i,1] <- nrow(geno_i) # nb d'individus croisement

    # calculer les fq des mk
    fq_all_i<- apply(geno_i, 2,
                     FUN = function (x) sum(x) / (2* length(x)))

    list_fq[[i]] <- fq_all_i # list des fq all pour chak cross

  }


  list_fq_sel <- vector(mode = "list", length = n_cr)
  for (i in 1:n_cr){

    # savoir quels mk ségrègent (>=10%) dans chak cross :
    list_fq_sel[[i]] <- list_fq[[i]][list_fq[[i]] >= threshold]

    # pour récup le nb de mk qui ségrègent (>= 10%) dans chak cross :
    nb_mk_cross[i,2] <- length(list_fq_sel[[i]])
  }
  colnames(nb_mk_cross) <- c("nb_ind", "mk_seg")
  rownames(nb_mk_cross) <- cr_id


  if (mk_commun == TRUE){

    # conserver mk qui segregent au dela du seuil
    # dans TOUS les croisements :
    # donc les mk communs :
    # marqueurs_communs <- intersect(names(list_fq_sel[[1]]),
    #                                names(list_fq_sel[[2]]))

    # Initialisation avec les noms des marqueurs
    # du premier élément de la liste
    mk_communs <- names(list_fq_sel[[1]])

    # Parcourir tous les autres éléments de la liste pour trouver les intersections
    for (i in 2:length(list_fq_sel)) {
      mk_communs <- intersect(mk_communs,
                                     names(list_fq_sel[[i]]))
    }

    list_mk_sel <- data.frame(nom_mk = mk_communs )

  } else{

    # remettre tous les croisements à la suite dans un df
    tab_fq_sel <- data.frame(nom_mk = character())
    for (i in 1:n_cr){
      df_i <- data.frame(nom_mk = names(list_fq_sel[[i]]))
      tab_fq_sel <- rbind(tab_fq_sel, df_i )
    }

    # conserver les mk ayant une fq all > 10%
    # dans au moins 1 croisement
    # cad enlever les doublons des noms de mk
    list_mk_sel <- unique(tab_fq_sel)

  }

  # selectionner les mk dans la matrice de geno et dans la carte
  geno_sel <- geno[,list_mk_sel$nom_mk]
  map_sel <- map[list_mk_sel$nom_mk, ]


  return(list( geno_sel = geno_sel,
               map_sel = map_sel,
               nb_mk_cross = nb_mk_cross))
}
