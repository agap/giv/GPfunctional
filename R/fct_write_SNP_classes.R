#' write_SNP_classes
#'
#' @description Write file describing the classes in which each SNP falls
#'
#' @param map \code{data.frame} with marker in row and marker identifier,
#' chromosome, genetic position (cM) and physical position (bp).
#'
#' @param mk_class \code{list} of \code{numeric} vector indicating the list of SNP
#' position per class.
#'
#' @param path \code{character} string specifying the path where data need
#' to be saved.
#'
#' @return snpmodel file written at the path location
#'
#' @examples
#'
#'\dontrun{
#' data("geno", package = "GPfunctional")
#' data("map", package = "GPfunctional")
#'
#' # sim function was modified (do not work)
#' y_sim <- GPF_sim_pheno(X = geno, map = map)
#' mk_class <-
#' write_SNP_classes(map = map, mk_class = mk_class,
#' path = "./software/bayesR/data")
#'
#' }
#'
#' @export
#'

write_SNP_classes <- function(map, mk_class, path){

  n_mk_cl <- length(mk_class)

  snpmodel <- matrix(n_mk_cl, nrow = nrow(map), ncol = 2, byrow = TRUE)

  for(i in 1:(n_mk_cl - 1)){

    snpmodel[mk_class[[i]], ] <- c(i, i)

  }

  write.table(x = snpmodel, file = file.path(path, "snpmodel"),
              na = "", col.names = FALSE, row.names = FALSE)

}
