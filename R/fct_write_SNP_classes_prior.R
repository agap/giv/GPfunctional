#' write_SNP_classes_prior
#'
#' @description Write file describing prior distributions of the normal mixtures
#' corresponding to each SNP effect classes.
#'
#' @param SNP_classes_prior \code{list} with each element corresponding to the
#' normal mixture information of a SNP class. Each element is a vector specifying:
#' number of components, the proportions of genetic variance explained (% VG_SNP_i)
#' and the prior of the Dirichlet distribution. See notes for details.
#'
#' @param path \code{character} string specifying the path where data need
#' to be saved.
#'
#' @note
#' The mixture distributions specified in `param SNP_classes_prior` are specified
#' using a vector taking the following information c(number of components,
#' prop var cl1, prop variance cl2, ..., Dirichlet delta prior).
#' For example, c(4, 0, 0.01, 0.001, 0.0001, 1,1,1,1), correspond to the default mixture
#' prior of the BayesR function with
#'
#' \eqn{p(\beta_j|\bm{\pi}, \sigma_g^{2}) = \pi_1 \times N(0, 0 \times \sigma_g^{2}) + ... +
#' \pi_4 \times N(0, 0.0001 \times \sigma_g^{2})}.
#'
#' and
#'
#' \eqn{
#' 	p(\pi_1, \pi_2, \pi_3, \pi_4) \sim D(1, 1, 1, 1)
#' }
#'
#' @examples
#' SNP_classes_prior <- list(cl_1 = c(2, 0, 0.01, 1,1),
#' cl_2 = c(4, 0, 0.01, 0.001, 0.0001, 1,1,1,1))
#'
#' \dontrun{
#' write_SNP_classes_prior(SNP_classes_prior = SNP_classes_prior,
#' path = "./software/bayesR/data")
#' }
#'
#' @return The return value, if any, from executing the function.
#'
#' @export


write_SNP_classes_prior <- function(SNP_classes_prior, path) {

  max_length <- max(unlist(lapply(SNP_classes_prior, FUN = length)))

  seg_mat <- matrix(NA, nrow = length(SNP_classes_prior),
                    ncol = max_length)
  for(i in 1:length(SNP_classes_prior)){

    seg_mat[i, 1:length(SNP_classes_prior[[i]])] <- SNP_classes_prior[[i]]

  }

  write.table(x = seg_mat, file = file.path(path, "seg"),
              na = "", col.names = FALSE, row.names = FALSE)

}
