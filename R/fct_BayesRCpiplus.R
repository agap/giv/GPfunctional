#' BayesRCpiplus
#'
#' @description Function to calculate BayesRCpi or BayesRC+ model using
#' the BayesRCO program from Mollandin et al. (2022).
#'
#' @param soft_exc_loc \code{character} string giving the path to the bayesR
#' executable file with data and result folder architecture. Default =
#' "./software/bayesR"
#'
#' @param geno genotype marker score matrix with genotype as row and marker as
#' columns. Marker scores must be scored 0, 1, 2 corresponding to the allele
#' frequency on one allele (generally minor).
#'
#' @param map corresponding marker map data frame with marker names chromosome
#' genetic position, physical position.
#'
#' @param pheno numeric vector of phenotypic values.
#'
#' @param mk_class List of the marker position (numeric) organised per class.
#' One element per marker class.
#'
#' @param prior_SNP_var Prior variance of the SNP effect (Va). parameter from a scaled
#' inverse chi-squared distribution. Default = 0.01
#'
#' @param prior_err_var Prior variance of the error (Ve). parameter from a scaled
#' inverse chi-squared distribution. Default = 0.01
#'
#' @param df_SNP_var degree of freedom of Va in the scaled inverse chi-squared
#' distribution. Problem with direct assignment of negative values. Default = -2
#'
#' @param df_err_var degree of freedom of Ve in the scaled inverse chi-squared
#' distribution. Problem with direct assignment of negative values. Default = -2
#'
#' @param prior_prop_Dirichlet vector of delta parameter for the mixture proportion
#' Dirichlet prior distribution. Default = c(1, 1, 1, 1).
#'
#' @param msize number of SNPs in reduced update. Default = 0
#'
#' @param mrep number of full cycles in reduced update. Default = 5000
#'
#' @param numit length of MCMC chain. Default = 5000
#'
#' @param burnin burnin steps. Default = 1000
#'
#' @param thin thinning rate. Default = 10
#'
#' @param ndist number of mixture distributions
#'
#' @param var_prop_mix proportion of the genetic variance explained by the
#' different mixture distribution. Default = c(0.0,0.0001,0.001,0.01).
#'
#' @param seed initial value for random number. Default = 0
#'
#' @param predict \code{logical} value specifying if prediction
#' should be realized. Default = TRUE.
#'
#' @param covar optional design matrix for covariates. Default = NULL
#'
#' @param additive \code{logical} value specifying if BayesRCpi (FALSE) or
#' BayesRC+ (TRUE) should be realized. Default = FALSE for BayesRCpi.
#'
#' @return list with  : marker effects (B_mk),
#' marker effect class frequencies (B_cl_freq),
#' genetic value predictions (g)
#'
#' @references
#'
#' Mollandin et al. (2022) Accounting for overlapping annotations in
#' genomic prediction models of complex traits. BMC Bioinformatics,
#' 23(365). https://doi.org/10.1186/s12859-022-04914-5
#'
#' @examples
#'
#' \dontrun{
#' data("geno", package = "GPfunctional")
#' data("map", package = "GPfunctional")
#' # phenotypes simulated
#' pheno_sim <- GPF_sim_pheno(X = geno, map = map)
#'
#' # set working directory with bayesR executable file
#' setwd("C:/Users/lacie/OneDrive/Documents/M2 DATA ANALYST/STAGE/BCNAM_Data/GP_functional")
#'
#' ## Non-overlapping annotations :
#' # List of the marker position (numeric) organised per class :
#' mk_class <- list(c1 = which(map$mk.names %in% pheno_sim$mk_sel_f$mk.names),
#'                  c2 = which(!(map$mk.names %in% pheno_sim$mk_sel_f$mk.names)))
#'
#'
#' ## Overlapping annotations (here, more than 2 classes) :
#' cl1 <- sample(1:ncol(geno), 100)
#' cl2 <- c(1:100)
#' cl3 <- (1:ncol(geno))[-cl1]
#'
#' mk_class <-list(c1 = cl1,
#'                 c2 = cl2,
#'                 c3 = cl3)
#'
#' ## BayesRCpi :
#'
#' ### without prediction :
#' res_BayesRCpi <- BayesRCpiplus(geno = geno, map = map,
#'                                pheno = pheno_sim$d_y$y_sim,
#'                                mk_class = mk_class,seed = 2108,
#'                                predict = FALSE)
#'
#' g_hat <- res_BayesRCpi$g
#'
#' # breeding values simulated :
#' bv_sim <- pheno_sim$d_y$y_f + pheno_sim$d_y$y_r
#'
#' plot(x = g_hat[, 1],y = bv_sim)
#' cor(x = g_hat[, 1],y = bv_sim)
#'
#' ### with prediction :
#' bv_sim2 <- bv_sim
#' set.seed(2108)
#' nbalea<-sample(1:100,20)
#' bv_sim2[nbalea]<-NA # phenotypes to predict are encoded 'NA'
#'
#' res_BayesRCpi_pred <- BayesRCpiplus(geno = geno, map = map,
#'                                     pheno = pheno_sim$d_y$y_sim,
#'                                     mk_class = mk_class,seed = 2108,
#'                                     predict = TRUE)
#' g_hat2 <- res_BayesRCpi_pred$g
#' plot(x = g_hat2[nbalea, 1],y = bv_sim[nbalea])
#' cor(x = g_hat2[nbalea, 1],y = bv_sim[nbalea])
#' # cor between breeding values predicted and breeding values simulated
#'
#' ## BayesRC+:
#'
#' ### without prediction :
#' res_BayesRCplus <- BayesRCpiplus(geno = geno, map = map,
#'                                  pheno = pheno_sim$d_y$y_sim,
#'                                  mk_class = mk_class,seed = 2108,
#'                                  predict = FALSE,
#'                                  additive = TRUE)
#'
#' g_hat <- res_BayesRCplus$g
#'
#' # breeding values simulated :
#' bv_sim <- pheno_sim$d_y$y_f + pheno_sim$d_y$y_r
#'
#' plot(x = g_hat[, 1],y = bv_sim)
#' cor(x = g_hat[, 1],y = bv_sim)
#'
#' ### with prediction :
#' bv_sim2 <- bv_sim
#' set.seed(2108)
#' nbalea<-sample(1:100,20)
#' bv_sim2[nbalea]<-NA # phenotypes to predict are encoded 'NA'
#'
#' res_BayesRCplus_pred <- BayesRCpiplus(geno = geno, map = map,
#'                                       pheno = pheno_sim$d_y$y_sim,
#'                                       mk_class = mk_class,seed = 2108,
#'                                       additive = TRUE,
#'                                       predict = TRUE)
#' g_hat2 <- res_BayesRCplus_pred$g
#' plot(x = g_hat2[nbalea, 1],y = bv_sim[nbalea])
#' cor(x = g_hat2[nbalea, 1],y = bv_sim[nbalea])
#' # cor between breeding values predicted and breeding values simulated
#' }
#'
#' @import genio
#' @import qgg
#' @importFrom stats rnorm
#' @importFrom utils read.table write.table
#'
#' @export


BayesRCpiplus <- function(geno, map, pheno, mk_class,
                       soft_exc_loc = "./software/bayesRCO",
                       prior_SNP_var = 0.01,
                       prior_err_var = 0.01,
                       df_SNP_var = NULL,
                       df_err_var = NULL,
                       prior_prop_Dirichlet = 1,
                       msize = 0,
                       mrep = 5000,
                       numit = 1000, burnin = 500,
                       thin = 10,
                       ndist = 4,
                       var_prop_mix = c(0.0,0.0001,0.001,0.01),
                       seed = 0,
                       predict = FALSE,
                       additive = FALSE
){

  # checks ----

  if(!is.null(df_SNP_var)){
    if(df_SNP_var < 0){stop("The program does not work for negative values of df_SNP_var")}
  }

  if(!is.null(df_err_var)){
    if(df_err_var < 0){stop("The program does not work for negative values of df_err_var")}
  }

  # process some info ----
  bayesR_path <- file.path(soft_exc_loc, "bayesRCO")
  bfile <- file.path(soft_exc_loc, "data/d")
  out <- file.path(soft_exc_loc, "results/res")
  pheno_pos <- 1
  n_class <- length(mk_class)

  # SNP_classes_prior <- file.path(soft_exc_loc, "data/seg")
  # gen_var_priors <- file.path(soft_exc_loc, "data/varcomp")
  data_loc <- file.path(soft_exc_loc, "data")

  # create the plink files
  create_plink(X = geno, map = map, y = pheno, path = data_loc)

  # create annotation file
  # (non-overlapping annotations or overlapping annotations)
  # if the SNP is in the categorie : 1 else 0
  # ncol = number of annotation class
  # nrow = number of SNP
  annot_mat_loc <- file.path(soft_exc_loc, "data/annot_mat")
  annot_mat <- matrix(data = 0, nrow = nrow(map), ncol = n_class)
  for(i in 1:n_class){annot_mat[mk_class[[i]], i] <- 1}
  write.table(x = annot_mat, file = annot_mat_loc,
              na = "", col.names = FALSE, row.names = FALSE)


  # write_SNP_classes(map = map, mk_class = mk_class, path = data_loc)
  # write_gen_var_prior(V_gen_prior, path = data_loc)
  #
  # std_mixture <- c(4, 0, 0.01, 0.001, 0.0001, 1,1,1,1)
  # SNP_cl_prior <- vector(mode = "list", length = n_class)
  # for(i in 1:n_class){SNP_cl_prior[[i]] <- std_mixture}
  # names(SNP_cl_prior) <- paste0("cl_", 1:n_class)
  #
  # write_SNP_classes_prior(SNP_classes_prior = SNP_cl_prior, path = data_loc)

  # create the executable command ----
  fct_arg <- c(bfile, out, pheno_pos, prior_SNP_var, prior_err_var, msize, mrep,
               numit, burnin, thin, ndist, seed, n_class, annot_mat_loc)

  arg_vect <- c("-bfile", "-out", "-n", "-vara", "-vare",
                "-msize", "-mrep", "-numit", "-burnin", "-thin", "-ndist",
                "-seed", "-ncat", "-catfile")

  cmd <- paste(arg_vect, fct_arg, collapse = " ")

  # add arguments with multiple values
  # p_Dir_arg <- paste0('-delta ', paste0(prior_prop_Dirichlet, collapse = ","))
  p_var_prop <- paste0('-gpin ', paste0(var_prop_mix, collapse = ","))
  cmd <- paste(cmd, p_var_prop)

  if (additive == TRUE) {
    print("BayesRC+")
    cmd2 <- paste(cmd, "-additive")
    # execute the bash script and collect the results ----
    system(command = paste(bayesR_path, cmd2),  intern = TRUE)
  } else {
    print("BayesRCpi")
    # execute the bash script and collect the results ----
    system(command = paste(bayesR_path, cmd),  intern = TRUE)
  }

  # get the estimates
  B_mk <- read.table(file = "./software/BayesRCO/results/res.param", header = T)
  B_cl_freq <- B_mk[, 1: ndist]
  B_mk <- B_mk$beta

  if(predict == TRUE){
    # possibility to do a prediction function
    # with phenotypes to predict encoded 'NA'
    # B_mk <- read.table(file = "./software/bayesR/results/res.param", header = T)
    mk_freq <- read.table(file = "./software/bayesR/results/res.frq")
    # B_mk <- B_mk$alpha
    pj <- mk_freq[, 1] # refer mk freq
    X <- read_bed(file = "./software/bayesR/data/d.bed", m_loci = length(B_mk),
                  n_ind = nrow(geno))
    X <- t(X)
    X_freq <- rep(1, nrow(X)) %*% t(pj)
    W <- sweep((X - (2*X_freq)), 2, sqrt(2*pj*(1-pj)), FUN = '/')
    # get the predicted genomic values : manually
    GV <-  W %*% (-1*B_mk)

  } else {
    # get the predicted genomic values directly from the file res.gv (bayesRCO)
    GV <- read.table(file = "./software/bayesRCO/results/res.gv")
  }

  return(list(B_mk = B_mk, B_cl_freq = B_cl_freq, g = GV))

}
