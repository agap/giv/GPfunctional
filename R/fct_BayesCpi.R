#' BayesCPi
#'
#' @description Function to calculate BayesCpi with or without featured
#' SNP. (use BGLR from BGLR package).
#'
#' @param geno genotype marker score matrix for featured markers
#' with genotype as row and marker as columns.
#' Marker scores must be scored 0, 1, 2 corresponding to the allele
#' frequency on one allele (generally minor).
#'
#' @param data_pheno data frame with $id for genotypes id and $pheno for
#' phenotypic values.
#'
#' @return list with: a data frame (1 column) with prediction of
#' breeding values (pred)
#'
#' @references
#'
#' BGLR package : Gustavo de los Campos, Paulino Perez Rodriguez,
#'
#' @examples
#'
#' \dontrun{
#'
#'res_BayesCPi <- BayesCpi(geno, data_pheno = d_i,
#'nb_iter = 1000, burnin = 100,
#'sauvegarde= "~/M2 DATA ANALYST/STAGE/BCNAM_Data/Scripts_Data/Simulations/MULTI_CROSS/BayesC/test")
#' }
#'
#' @import BGLR
#'
#' @export


BayesCpi<- function(geno, data_pheno, nb_iter = 10000,
                    burnin = 1000, sauvegarde= "",
                    verbose = FALSE){

  res <- BGLR(y=data_pheno$pheno,
              ETA=list(list(X=geno,model='BayesC')),
              nIter= nb_iter, burnIn= burnin,
              saveAt=sauvegarde,
              verbose = FALSE)

  pred_B<-data.frame(pred_BCpi = res$yHat)
  rownames(pred_B)<-rownames(geno)

  return(list(pred = pred_B))

}

